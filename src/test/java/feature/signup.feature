@Signup
Feature: User SignUp

  Scenario: User can sign up successfully
    Given User is on the registration page
    When User enters valid username and valid email
    And User fills his details and clicks on create account button
    Then User should be deregistered and redirected to the home page