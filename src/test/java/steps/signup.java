package steps;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;  // Drop down list library package
import org.openqa.selenium.JavascriptExecutor;

import java.time.Duration;


public class signup {
    public static WebDriver driver;


    @Given("User is on the registration page")
    public void user_is_on_the_registration_page() {

        System.setProperty("webdriver.chrome.driver","src/main/resources/chromedriver.exe");
        driver = new ChromeDriver();
        driver.get("https://automationexercise.com/login");

    }
    @When("User enters valid username and valid email")
    public void user_enters_valid_username_and_valid_email() {

        //defining the user text boxes and to type/send data in the text box
        WebElement usernameField = driver.findElement(By.xpath("//*[@id=\"form\"]/div/div/div[3]/div/form/input[2]"));
        usernameField.sendKeys("test");

        WebElement emailField = driver.findElement(By.xpath("//*[@id=\"form\"]/div/div/div[3]/div/form/input[3]"));
        emailField.sendKeys("testppo33@gmail.com");



        //user clicks on signup button
        WebElement signUpButton = driver.findElement(By.xpath("//*[@id=\"form\"]/div/div/div[3]/div/form/button"));
        signUpButton.click();

        //just want to make sure that user is now in the right page
        Assert.assertEquals(driver.getTitle(),"Automation Exercise - Signup");

    }
    @When("User fills his details and clicks on create account button")
        public void User_fills_his_details_and_clicks_on_create_account_button() {

        WebElement user_title = driver.findElement(By.xpath("//*[@id=\"id_gender1\"]"));
        user_title.click();

        WebElement type_password = driver.findElement(By.xpath("//*[@id=\"password\"]"));
        type_password.sendKeys("2017");

        WebElement firstname = driver.findElement(By.xpath("//*[@id=\"first_name\"]"));
        firstname.sendKeys("Fname");

        WebElement lastname = driver.findElement(By.xpath("//*[@id=\"last_name\"]"));
        lastname.sendKeys("Lname");

        Select drpCountry = new Select(driver.findElement(By.xpath("//*[@id=\"country\"]")));
        drpCountry.selectByVisibleText("United States");

        WebElement address = driver.findElement(By.xpath("//*[@id=\"address1\"]"));
        address.sendKeys("17 G.street,11");

        WebElement state = driver.findElement(By.xpath("//*[@id=\"state\"]"));
        state.sendKeys("Holt");

        WebElement city = driver.findElement(By.xpath("//*[@id=\"city\"]"));
        city.sendKeys("Mi");

        WebElement zipcode = driver.findElement(By.xpath("//*[@id=\"zipcode\"]"));
        zipcode.sendKeys("54321");

        WebElement mobile_number = driver.findElement(By.xpath("//*[@id=\"mobile_number\"]"));
        mobile_number.sendKeys("0123456789");

        driver.manage().window().maximize();  // max the browser screen

        // i had to use the scroll cuz the button navigation location on the page is not accurate
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("window.scrollBy(0,500)");

        // click on create account button
        WebElement createAccount = driver.findElement(By.xpath("//*[@id=\"form\"]/div/div/div/div[1]/form/button"));
        createAccount.click();


    }
    @Then("User should be deregistered and redirected to the home page")
    public void user_should_be_deregistered_and_redirected_to_the_home_page() {


        Assert.assertEquals(driver.getTitle(),"Automation Exercise - Account Created");

        WebElement continueButton = driver.findElement(By.xpath("//*[@id=\"form\"]/div/div/div/div/a"));
        continueButton.click();

       //wait for 2 seconds
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        driver.navigate().refresh(); //to avoid potential annoying ads, also you can install an ad blocker


        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        //The below code is for logging out however it already exists in a feature (issue here )
        WebElement Logout = driver.findElement(By.xpath("//*[@id=\"header\"]/div/div/div/div[2]/div/ul/li[4]/a"));
        Logout.click();



        Assert.assertEquals(driver.getTitle(),"Automation Exercise - Signup / Login");
        driver.quit();

    }
}
